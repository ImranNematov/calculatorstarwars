﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace UnitTestProject8
{
    class Avtotest
    {

        [TestCase("*")]
        [TestCase("/")]
        [TestCase("(")]
        [TestCase(")")]
        [TestCase("^")]
        [TestCase("+")]
        [TestCase("-")]
        public void TestZnaki(string a)
        {
            IWebDriver driver = new ChromeDriver();
            driver.Manage().Window.Maximize();
            driver.Navigate().GoToUrl("file:///D:/calculatorstarwars/calculatorStarWars/index.html");
            driver.FindElement(By.XPath($"//button[@value='{a}']")).Click();
            IWebElement flashMessage = driver.FindElement(By.XPath("//Input[@type='text']"));
            string actual = flashMessage.GetAttribute("value");
            Assert.AreEqual(a, actual);

            driver.Quit();
        }

        [TestCase("1")]
        [TestCase("2")]
        [TestCase("3")]
        [TestCase("4")]
        [TestCase("5")]
        [TestCase("6")]
        [TestCase("7")]
        [TestCase("8")]
        [TestCase("9")]
        [TestCase("0")]


        public void TestCifri(string a)
        {
            IWebDriver driver = new ChromeDriver();
            driver.Manage().Window.Maximize();
            driver.Navigate().GoToUrl("file:///D:/calculatorstarwars/calculatorStarWars/index.html");
            driver.FindElement(By.XPath($"//button[@value='{a}']")).Click();
            IWebElement flashMessage = driver.FindElement(By.XPath("//Input[@type='text']"));
            string actual = flashMessage.GetAttribute("value");
            Assert.AreEqual(a, actual);
            driver.Quit();

        }


        [TestCase(-50, 15, -35)]
        [TestCase(114, 458, 572)]
        [TestCase(854, 124, 978)]
        [TestCase(965, 95, 1060)]
        [TestCase(452, 52, 504)]
        [TestCase(-64, 2, -62)]
        [TestCase(10000, 1100, 11100)]


        public void TestPlus(double a, double b, double c)
        {
            IWebDriver driver = new ChromeDriver();
            driver.Manage().Window.Maximize();
            driver.Navigate().GoToUrl("file:///D:/calculatorstarwars/calculatorStarWars/index.html");
            foreach (char ch in a.ToString())
            {
                driver.FindElement(By.XPath($"//button[@value='{ch}']")).Click();
            }
            driver.FindElement(By.XPath("//button[@value='+']")).Click();
            foreach (char ch in b.ToString())
            {
                driver.FindElement(By.XPath($"//button[@value='{ch}']")).Click();
            }
            driver.FindElement(By.XPath("//button[@value='=']")).Click();
            IWebElement flashMessage = driver.FindElement(By.XPath("//Input[@type='text']"));
            string actual = flashMessage.GetAttribute("value");
            Assert.AreEqual(c.ToString(), actual);
            driver.Quit();


        }

        [TestCase(8, 8, 0)]
        [TestCase(110, 52, 58)]
        [TestCase(52, 68, -16)]
        [TestCase(79, 9, 70)]
        [TestCase(254, 114, 140)]
        [TestCase(965, 0, 965)]
        [TestCase(8564, 965, 7599)]
        

        public void TestMinus(double a, double b, double c)
        {
            IWebDriver driver = new ChromeDriver();
            driver.Manage().Window.Maximize();
            driver.Navigate().GoToUrl("file:///D:/calculatorstarwars/calculatorStarWars/index.html");
            foreach (char ch in a.ToString())
            {
                driver.FindElement(By.XPath($"//button[@value='{ch}']")).Click();
            }
            driver.FindElement(By.XPath("//button[@value='-']")).Click();

            foreach (char ch in b.ToString())
            {
                driver.FindElement(By.XPath($"//button[@value='{ch}']")).Click();
            }
            driver.FindElement(By.XPath("//button[@value='=']")).Click();

            IWebElement flashMessage = driver.FindElement(By.XPath("//Input[@type='text']"));
            string actual = flashMessage.GetAttribute("value");
            Assert.AreEqual(c.ToString(), actual);

            driver.Quit();

        }

       
        
        [TestCase(96, 5, 480)]
        [TestCase(10000, 60, 600000)]
        [TestCase(85, 52, 4420)]
        [TestCase(658, 632, 415856)]
        [TestCase(-746, 85, -63410)]
        [TestCase(4587, 62, 284394)]
        [TestCase(5247, 482, 2529054)]


        public void TestUmnojit(double a, double b, double c)
        {
            IWebDriver driver = new ChromeDriver();
            driver.Manage().Window.Maximize();
            driver.Navigate().GoToUrl("file:///D:/calculatorstarwars/calculatorStarWars/index.html");
            foreach (char ch in a.ToString())
            {
                driver.FindElement(By.XPath($"//button[@value='{ch}']")).Click();
            }
            driver.FindElement(By.XPath("//button[@value='*']")).Click();

            foreach (char ch in b.ToString())
            {
                driver.FindElement(By.XPath($"//button[@value='{ch}']")).Click();
            }
            driver.FindElement(By.XPath("//button[@value='=']")).Click();

            IWebElement flashMessage = driver.FindElement(By.XPath("//Input[@type='text']"));
            string actual = flashMessage.GetAttribute("value");
            Assert.AreEqual(c.ToString(), actual);

            driver.Quit();
        }

        [TestCase(25, 5, 5)]
        [TestCase(50, 0, "Infinity")]
        [TestCase(0, 196, 0)]
        [TestCase(512, 2, 256)]
        [TestCase(95, 5, 19)]
        [TestCase(1254, 6, 209)]
        [TestCase(1547, 7, 221)]


        public void TestDeleniye(double a, double b, object c)
        {
            IWebDriver driver = new ChromeDriver();
            driver.Manage().Window.Maximize();
            driver.Navigate().GoToUrl("file:///D:/calculatorstarwars/calculatorStarWars/index.html");

            foreach (char ch in a.ToString())
            {
                driver.FindElement(By.XPath($"//button[@value='{ch}']")).Click();
            }
            driver.FindElement(By.XPath("//button[@value='/']")).Click();

            foreach (char ch in b.ToString())
            {
                driver.FindElement(By.XPath($"//button[@value='{ch}']")).Click();
            }
            driver.FindElement(By.XPath("//button[@value='=']")).Click();

            IWebElement flashMessage = driver.FindElement(By.XPath("//Input[@type='text']"));
            string actual = flashMessage.GetAttribute("value");
            Assert.AreEqual(c.ToString(), actual);

            driver.Quit();
        }

        [TestCase(74, 2, 5476)]
        [TestCase(25, 3, 15625)]
        [TestCase(7, 7, 823543)]
        [TestCase(0, 10, 0)]
        [TestCase(785, 0, 1)]
        [TestCase(1, 300, 1)]
        [TestCase(6, 5, 7776)]
        [TestCase(96, 2, 9216)]


        public void TestStepen(double a, double b, double c)
        {
            IWebDriver driver = new ChromeDriver();
            driver.Manage().Window.Maximize();
            driver.Navigate().GoToUrl("file:///D:/calculatorstarwars/calculatorStarWars/index.html");
            foreach (char ch in a.ToString())
            {
                driver.FindElement(By.XPath($"//button[@value='{ch}']")).Click();
            }
            driver.FindElement(By.XPath("//button[@value='^']")).Click();

            foreach (char ch in b.ToString())
            {
                driver.FindElement(By.XPath($"//button[@value='{ch}']")).Click();
            }
            driver.FindElement(By.XPath("//button[@value='=']")).Click();

            IWebElement flashMessage = driver.FindElement(By.XPath("//Input[@type='text']"));
            string actual = flashMessage.GetAttribute("value");
            Assert.AreEqual(c.ToString(), actual);

            driver.Quit();
        }

        
       
        [TestCase('(',53, '+' ,60,')', '*',3,339)]
        [TestCase('(', 65, '-', 60, ')', '+', 457, 462)]
        [TestCase('(', 110, '*', 2, ')', '/', 20, 11)]
        public void TestSkobkami(char f,double a, char g,double b, char h, char j,double c, double d)
        {
            IWebDriver driver = new ChromeDriver();
            driver.Manage().Window.Maximize();
            driver.Navigate().GoToUrl("file:///D:/calculatorstarwars/calculatorStarWars/index.html");
            driver.FindElement(By.XPath($"//button[@value='{f}']")).Click();
            foreach (char ch in a.ToString())
            {
                driver.FindElement(By.XPath($"//button[@value='{ch}']")).Click();
            }
            driver.FindElement(By.XPath($"//button[@value='{g}']")).Click();
            foreach (char ch in b.ToString())
            {
                driver.FindElement(By.XPath($"//button[@value='{ch}']")).Click();
            }
            driver.FindElement(By.XPath($"//button[@value='{h}']")).Click();
            driver.FindElement(By.XPath($"//button[@value='{j}']")).Click();
            foreach (char ch in c.ToString())
            {
                driver.FindElement(By.XPath($"//button[@value='{ch}']")).Click();
            }
            driver.FindElement(By.XPath("//button[@value='=']")).Click();
            IWebElement flashMessage = driver.FindElement(By.XPath("//Input[@type='text']"));
            string actual = flashMessage.GetAttribute("value");
            Assert.AreEqual(d.ToString(), actual);
            driver.Quit();
        }


    }
}
